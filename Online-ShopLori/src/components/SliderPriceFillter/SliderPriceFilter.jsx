import React, {useState} from "react";
import { Slider } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
const RangeSliderPrice = styled(Slider)({
    height: '4px',
    borderRadius: '5px',
    color: '#AFDFC2'
});
const SliderPriceFilter = () =>{
    const [state, setState] = useState(1000)
    const updateRange = (e, data) =>{
        setState(data)
    }
    return(
        <>
            <h4>Цена</h4>
            <RangeSliderPrice value={state} onChange={updateRange} defaultValue={0} max={10000}/>
            {state}
            </>
    )
}

export default SliderPriceFilter