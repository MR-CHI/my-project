import React from "react";
import './dropdawn.scss'

const DropDown = ({onClick, className, flag}) =>{

    // const active = flag ? {display: 'none'} : {display: 'block'}
    return(
        <div  onClick={onClick} className={className}>
            <div className="containerDrop" >
                <div className="bar1"></div>
                <div className="bar2"></div>
                <div className="bar3"></div>
            </div>
        </div>

    )
};
export default DropDown
