import React, {useEffect} from 'react';
import './header-style.scss';
import Navbar from "../Navbar/components/Navbar/Navbar";
import Logo from "../../Logo/Logo";
import LiveSearch from "../HeaderLiveSearch/LiveSearch";
import LoginProfile from "../../Auth/LoginProfile/LoginProfile";
import HeaderCategory from "../HeaderCategory/HeaderCategory";
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import {NavLink} from "react-router-dom";
import {useDispatch} from 'react-redux';
import {actionNavChange} from "../../../app/store/Header/actions/actionNavChange";
import DropDown from "../../DropDown/DropDown";
import ModalMenu from "../ModalMenu/components/ModalMenu";
import {addFromLocalStorage} from "../../../app/store/Cart/actions/addFromLocalStorageAction";

const Header = () => {
    const loading = useSelector(state => state.allProduct.loading);
    const isOpen = useSelector(state => state.isOpen.isOpen);
    const count = useSelector(state => state.cart.cartList.length)
    const show = loading ? {display: 'none'} : {display: 'block'};
    const dispatch = useDispatch();
    const openNavbar = () => {
        const action = actionNavChange();
        dispatch(action)
    };
    useEffect(() => {
        const products = localStorage.getItem('cartProducts')
        if(products){
        dispatch(addFromLocalStorage(JSON.parse(products)))
        }

    }, [])

    return (
        <section style={show} className='header-container'>
            <ModalMenu flag={isOpen} onClick={openNavbar} />
            <div className='header-container_item'>
                <div className='container-content'>
                    <ul className='header-container_elements'>
                        <Navbar className={'header-container_links'}/>
                    </ul>
                    <div className={'header-container_language'}>
                        EN/UA
                    </div>
                </div>
            </div>
            <hr/>
            <div className='header-container_middle'>
                <div className='header-container_middle_items'>
                    <DropDown  onClick={openNavbar} className='containerDrop'/>
                    <Logo flag={true}  classNames={'logo-little'}/>
                    <LiveSearch/>
                    <LoginProfile/>
                    <NavLink to={'/cart'}>
                        <FontAwesomeIcon className='header-container_middle_items_cart' icon={faShoppingCart}/>
                        <span className={'header-container_middle_items_count'}>{count}</span>
                    </NavLink>
                </div>
            </div>
            <HeaderCategory/>
        </section>
    )
}
export default Header
