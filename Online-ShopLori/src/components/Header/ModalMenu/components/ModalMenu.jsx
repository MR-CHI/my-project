import React from "react";
import Button from "../../../Button/Button";
import Navbar from "../../Navbar/components/Navbar/Navbar";
import '../style/style-modalMenu.scss'
import HeaderCategory from "../../HeaderCategory/HeaderCategory";
import ModalList from "./ModalList/ModalList";



const ModalMenu = ({className, onClick, flag}) => {
    const active = flag ? {display: 'block'} : {display: 'none'}

    return(
        <div style={active} className={className}>
            <div  className='container-ModalMenu'>
                <div className='container-ModalMenu__header'>
                    <h4 className='ModalMenu-title'>Menu</h4>
                    <Button onClick={onClick} className='button-ModalMenu' text='X' />
                </div>
                <div className='ModalMenu-link__container'>
                    <Navbar close={onClick} className='ModalMenu-link' />
                    <h4>Категории товаров</h4>
                    <ModalList close={onClick} classNames='ModalMenu-link' />
                </div>
            </div>
        </div>
    )
}

export default ModalMenu
