export const category = [
    {
        name: 'Столы',
        productUrl: '/products/Столы'
    },
    {
        name: 'Стулья',
        productUrl: '/products/Стулья'
    },
    {
        name: 'Корпусная мебель',
        productUrl: '/products/Шкафы'
    },
    {
        name: 'Кровати',
        productUrl: '/products/Кровати'
    }
]
