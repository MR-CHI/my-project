import React from 'react';
import {category} from "./category";
import {NavLink} from "react-router-dom";
import {v4 as uuidv4} from "uuid";


const HeaderCategory = () => {
    const categories = category.map(({name, productUrl}) => {
        return (
            <React.Fragment key={uuidv4()}>
                <NavLink
                    className={'header-container-categories-label-name'}
                    to={`${productUrl}`}>
                    {name}</NavLink>
            </React.Fragment>)
    })
    return (
        <div className={'header-container-categories'}>
            <div className={'header-container-categories-label'}>
                {categories}
            </div>
        </div>
    );
};

export default HeaderCategory;
