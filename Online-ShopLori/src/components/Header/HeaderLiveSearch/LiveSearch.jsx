import React, { useState, useEffect } from 'react';
import axios from 'axios'
import './LiveSearch.scss'
import {NavLink} from "react-router-dom";
import {v4 as uuidv4} from "uuid";

const LiveSearch = () => {

    const [loading, setLoading] = useState(false)
    const [products, setProducts] = useState([])
    const [search, setSeartch] = useState('')
    const [filteredNames, setFilteredNames] = useState([])
    useEffect( () => {
        setFilteredNames(
        products.filter (products => {
            return products.name.toLowerCase().includes(search)
        })
    )
},[search , products])

    useEffect(() => {
        setLoading(true)
        axios.get('https://glacial-river-19645.herokuapp.com/api/products/')
            .then(response => {
                setProducts(response.data);
                setLoading(false)
            }).catch(e => {
            console.error(e)})
    },[]);

    // if(loading){<p>Загружаю....</p>}

    const filteredArr = filteredNames.map(({name, _id, imageUrls,categories,itemNo}) => {
        return (<NavLink  to={`/products/${categories}/${itemNo}`} key={uuidv4()} className={search ? 'search-item found' : 'search-item '}><li  className='hoverLi' onClick={()=>setSeartch('')} key ={_id}>{name} <img src={imageUrls[0]} alt={name}  /></li></NavLink>)
    })
    return(
        <div className='containerSearch' >
            <input className ='search-input'
                   type = 'text'
                   placeholder = 'Введите название товара'
                   onChange ={e => setSeartch(e.target.value)}
                   value ={search} name = 'search' />
            {/*<i className= 'fas fa-search'></i>*/}
            <ul className={!search ? 'search ' : 'search found'}>{filteredArr}</ul>
        </div>
    )
}

export default LiveSearch

