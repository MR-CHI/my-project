import React, {useReducer} from 'react';
import LoginForm from "../LoginForm/LoginForm";
import RegisterForm from "../RegisterForm/RegisterForm";
import './styleModalAuth.scss'
import {modalAuthReducer} from '../../../app/store/Auth/modalAuthReducer'
import {initialState} from '../../../app/store/Auth/modalAuthReducer'
// import {useSelector} from "react-redux/";

const ModalAuth = ({style}) =>{
    const [toggle, dispatch] = useReducer(modalAuthReducer, initialState);
    const result = toggle ? {display: 'block'} : {display: 'none'};
    const result2 = toggle ? {display: 'none'} : {display: 'block'};


    return(
        <div style={style} className='containerModalAuth'>
            <div className='containerModalAuth-container'>
                <div className='containerModalAuth-nav'>
                    <p className='containerModalAuth-nav__title activeColor'   onClick={()=>{ dispatch({type: 'CLOSE'})}}>Войти</p>
                    <p className='containerModalAuth-nav__title activeColor'  onClick={()=>dispatch({type: 'OPEN'})}>Регистрация</p>
                </div>
                <div>
                    <p style={result2}>
                        Пожалуйста, введите данные своей учетной записи, чтобы войти
                    </p>
                </div>
                <div>
                    <LoginForm style={result2}/>
                    <RegisterForm style={result} />
                </div>
            </div>
        </div>
    )
}
export default ModalAuth
