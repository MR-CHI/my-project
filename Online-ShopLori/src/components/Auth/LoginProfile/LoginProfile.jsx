import React, {useEffect} from 'react';
import './loginProfile.scss'
import {useDispatch} from "react-redux";
import {openModal} from "../../../app/store/Modal/actions/openModal";
import ModalAuth from "../ModalAuth/ModalAuth";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {addNewUser} from "../../../app/store/Auth/actions/addNewuser";

const LoginProfile = () => {
    const dispatch = useDispatch();
    useEffect(()=>{
        const customer = localStorage.getItem('customer');
        if(customer){
            dispatch(addNewUser(JSON.parse(customer)))
        }

    }, [])

    const flag = useSelector(state => state.addNewCustomer.flag);
    const nameUser = useSelector(state => state.addNewCustomer.user);
    const showNameUser  = flag ? nameUser.firstName + ', В личный кабинет' : null;
    const showIcon = flag ? {display: 'none'} : {display: 'block'};

    return (
            <div className='container-Profile-login' >
                <i style={showIcon} onClick={() => dispatch(openModal(<ModalAuth  />))} className="far fa-user" ></i>
                <NavLink className='showNameUser' to={'/userProfile'}>
                    {showNameUser}
                </NavLink>

            </div>
        );

}

export default LoginProfile;
