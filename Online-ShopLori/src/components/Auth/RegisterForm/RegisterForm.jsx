import React, { useState } from 'react';
import axios from 'axios'
import {fields} from "./registerFormFields";
import './RegisterForm.scss'
import Input from "../../Input/Input"
import {closeModal} from "../../../app/store/Modal/actions/closeModal";
import {useDispatch} from "react-redux";


const RegisterForm = ({style}) => {
    const dispatch = useDispatch()

    const [inputValues, setInputValues] = useState({})

    const handleOnChange = event => {
        const { name, value } = event.target;
        inputValues[name] = value
        setInputValues(inputValues);
    };

    const handleSubmit = (event)=>{
       event.preventDefault();
       const sendRequest = async () =>{
           try {
               const resp = await axios.post('https://glacial-river-19645.herokuapp.com/api/customers', inputValues,
                   {headers: {'Content-Type': 'application/json' }} )
               setInputValues({})
               dispatch(closeModal())
           } catch (error){
               alert(error.response.data.message && error.response.request.responseText)
               console.dir(error)
           }
       }
        sendRequest()
    };

    return(
            <form style={style} onSubmit={handleSubmit}>
                <Input  {...fields.firstName} onChange = {handleOnChange}  />
                <Input  {...fields.lastName} onChange={handleOnChange}/>
                <Input  {...fields.login} onChange = {handleOnChange} />
                <Input  {...fields.email}  onChange = {handleOnChange}/>
                <Input  {...fields.password} onChange = {handleOnChange} />
                <Input className='buttonAuth' {...fields.submit}/>
            </form>
        )

}

export default RegisterForm;
