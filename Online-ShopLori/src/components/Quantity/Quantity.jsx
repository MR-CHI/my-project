import React from 'react';
import Button from "../Button";
import './Quantity.scss'

const Quantity = ({counter, onClickMinus, onClickPlus}) => {
    return (
        <div className='quantity'>
            <Button className='quantity-button'
                    onClick={onClickMinus}
                    text={'-'}/>
            <div>{counter}</div>
            <Button className='quantity-button'
                    onClick={onClickPlus}
                    text={'+'}/>
        </div>
    );
};

export default Quantity;
