import React from "react";
import './Modal.scss';
import {useSelector, useDispatch} from "react-redux";
import {closeModal} from "../../app/store/Modal/actions/closeModal";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

const Modal = () => {
    const state = useSelector(state => state.modal)
    const modalIsOpen = state.modalIsOpen
    const product = state.product;
    const dispatch = useDispatch()
    const style = {
        display: modalIsOpen ? "flex" : "none"
    };
    return (
        <div className="modal" style={style}>
            <div className='modal-content'>
                <FontAwesomeIcon className='modal-content-close' icon={faTimes} onClick={()=>{dispatch(closeModal())}}/>
                {product}
            </div>
        </div>
    )
}

export default Modal
