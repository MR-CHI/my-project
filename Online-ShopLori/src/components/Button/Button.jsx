import React from 'react';

const Button = ({text, onClick, className = '', style})=> {
    return <button style={style} className={className} onClick={onClick} >{text}</button>
}

export default Button;
