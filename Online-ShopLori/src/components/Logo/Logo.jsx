import React from 'react';
import logo from '../../assets/images/logoImages/logo.png'
import {NavLink} from "react-router-dom";
import './logo-style.scss'


const Logo = ({number, flag, classNames, src = logo}) =>{
    const style = {
        display: flag ? 'block' : 'none'
    }

    return(
        <div className='logo-container'>
            <NavLink to={'/Home'} >
                <img className={classNames} src={src} alt="logo"/>
            </NavLink>
            {/*<div style={style} className='Header-container_numbers'>*/}
            {/*    /!*<h4 style={style}>{number}</h4>*!/*/}
            {/*    /!*<h4 style={style}>{number}</h4>*!/*/}
            {/*</div>*/}
        </div>
    )
}

export default Logo;
