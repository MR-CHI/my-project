import React from 'react';
import {fieldsColor} from './fields'
import Input from "../Input/Input";
import './styleFilter.scss'
import {useDispatch} from "react-redux";
import {clearFilter} from "../../app/store/Products/actions/clearFilter";
import {actionFilterProducts} from "../../app/store/Products/actions/actionFilterProducts";

const FilterProducts = () =>{
    const dispatch = useDispatch();
    const filterElements = fieldsColor.map(({type, name, value,text}) => {
        return  (
            <div className={'filterColorRadioContainer'}>
                <Input  name={name} type={type} value={value} text={text} onChange={(event) => dispatch(actionFilterProducts(event.target.value))} />
                </div>
            )
    })

    return(
        <div  className='containerFilter'>
            <div className='filterColorContainer'>
                <h3>Цвет</h3>
                {filterElements}
            </div>
            {/*<SliderPriceFilter />*/}
            <button className={'filterColorBtn'} onClick={() => dispatch(clearFilter([]))}>Очистить фильтр</button>
        </div>
    )
}

export default FilterProducts;
