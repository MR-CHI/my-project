import React from 'react'
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from "react-router-dom";
import ContextProvider from "../../context/Provider";
import AppRoutes from "../../routes/AppRoutes";
import store from '../../store/store'
import Footer from '../../../components/Footer/Footer'
import Header from "../../../components/Header/Header/Header";
import Container from "../../../components/Container/Container";
import Modal from "../../../components/Modal";

function App() {
    return (
        <Provider store={store}>
            <Router>
                <ContextProvider>
                    <Header />
                    <Container >
                        <AppRoutes/>
                    </Container>
                    <Footer />
                    <Modal/>
                </ContextProvider>
            </Router>
        </Provider>


    );
}

export default App;
