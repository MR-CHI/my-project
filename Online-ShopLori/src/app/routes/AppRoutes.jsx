import React from "react";
import {Switch, Route, Redirect } from 'react-router-dom'
import HomePage from "../../pages/Home/HomePage";
import CartPage from "../../pages/Cart/CartPage";
import OrderPage from "../../pages/Order/OrderPage/OrderPage";
import ComingPage from "../../pages/Coming/ComingPage";
import CategoryProductPage from "../../pages/Products/CategoryProductPage";
import ProductPage from "../../pages/Products/ProductPage";
import ProfilePage from "../../pages/Profile/ProfilePage/ProfilePage";
import PrivateRoute from "../../components/PrivatePoute/PrivateRoute";



const AppRoutes = () => {
    return(
        <Switch>
            <Route exact path='/'>
                <HomePage/>
            </Route>
            <Route exact path='/home'>
                <HomePage/>
            </Route>
            <Route exact path='/products/:category' component={CategoryProductPage}/>
            <Route exact path='/products/:category/:productName' component={ProductPage}/>

            <Route exact path='/orderingPage'>
                <OrderPage />
            </Route>

            <Route exact path='/cart'>
                <CartPage />
            </Route>

            <PrivateRoute component={()=> <ProfilePage />} path='/userProfile' />

            <Route exact path='*'>
                <ComingPage/>
            </Route>
            <Redirect from="/" to={<HomePage />} />
        </Switch>
    )
}

export default AppRoutes;
