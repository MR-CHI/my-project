import {createStore, combineReducers, applyMiddleware} from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import {productReducer} from "./Products/productReducer";
import {cartReducer} from "./Cart/cartReducer";
import {modalReducer} from "./Modal/modalReducer";
import {productViewReducer} from "../../pages/Products/components/ProductView/productViewReducer/productViewReducer";
import {headerReducer} from "./Header/headerReducer";
import {authReducer} from "./Auth/authReducer";

const rootReducer = combineReducers({
    allProduct: productReducer,
    cart: cartReducer,
    modal: modalReducer,
    activeImage: productViewReducer,
    isOpen: headerReducer,
    addNewCustomer: authReducer
})

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store
