export const profileReducer = (state, action) => {
    if (action.type === "ACTIVATE_TAB") {
        return state.map(item => {
            item.active = item.name === action.payload;
            return item;
        });
    }
    return state;
};
