import {ADD_NEW_USER, CLEAR_DATA_USER} from "./constants";

const initialState = {
    user: [],
    flag: false
};

export const authReducer = (state = initialState, {type, payload}) =>{
    switch (type) {
        case ADD_NEW_USER :
            return {...state, user: payload, flag: true}
        case CLEAR_DATA_USER :
            return {...state, user: payload, flag: false}

        default : return state
    }
}