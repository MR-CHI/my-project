import {ADD_NEW_USER} from "../constants";

export const addNewUser = (payload) =>{
    return {
        type: ADD_NEW_USER,
        payload
    }
};

