import {CLEAR_DATA_USER} from "../constants";

export const removeDataUser = (payload) =>{
    return{
        type: CLEAR_DATA_USER,
        payload
    }
}
