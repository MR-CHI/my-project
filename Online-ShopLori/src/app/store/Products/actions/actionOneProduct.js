import axios from "axios";
import { FETCH_PRODUCTS_REQUEST, ONE_PRODUCT, FETCH_PRODUCTS_FAILED} from "./actionType";

export const actionOneProductGet = (productName) =>{
    const func  = async (dispatch) =>{
        dispatch({type: FETCH_PRODUCTS_REQUEST})

        try{
            const response = await axios.get(`https://glacial-river-19645.herokuapp.com/api/products/${productName}`)
            const {data} = response;
            dispatch({type: ONE_PRODUCT, payload: data})
        } catch (error) {
            dispatch({type: FETCH_PRODUCTS_FAILED, payload: error})
        }
    }
    return func;
}
