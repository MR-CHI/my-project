import {
    FETCH_PRODUCTS_REQUEST,
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILED,
    FILTER_COLOR,
    CLEAR_FILTER,
    ONE_PRODUCT
} from "./actions/actionType";
import transitions from "@material-ui/core/styles/transitions";

const initialState = {
    page: {},
    product: [],
    errors: null,
    loading: false,
    filterProduct: [],
    oneProduct: [],
};

export const productReducer = (state = initialState, {type, payload}) =>{
    switch (type) {
        case  FETCH_PRODUCTS_REQUEST :
            return(
                {...state, loading: true}
            )
        case FETCH_PRODUCTS_SUCCESS :
            return {...state, loading: false, product: payload}

            case FETCH_PRODUCTS_FAILED :
            return (
                {...state, loading: false, errors: payload}
            )
        case FILTER_COLOR :
            return (
                {...state, value: payload}
            )
        case CLEAR_FILTER :
            return (
                {...state, filterProduct: []}
            )
        case 'ADD' :
            return (
                {...state, filterProduct: payload, loading: false}
            )
        case ONE_PRODUCT :
            return (
                {...state, oneProduct: payload, loading: false,}
            )
        default : return state
    }
}
