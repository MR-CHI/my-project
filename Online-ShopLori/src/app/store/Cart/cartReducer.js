import {
    ADD_CART,
    REMOVE_CART,
    MINUS_ITEM,
    PLUS_ITEM, ADD_FROM_LOCALSTORAGE, CLEAR_CART,
} from "./constants";
import {initialState} from './initialState'

export const cartReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ADD_CART:
            const indexItem = state.cartList.findIndex(item => item.itemNo === payload.itemNo)
            if (indexItem === -1) {
                payload.totalPrice = payload.counter * payload.currentPrice
                const newCartList = [...state.cartList, payload]
                localStorage.setItem('cartProducts', JSON.stringify(newCartList))
                return ({...state, cartList: newCartList})
            } else {
                payload.counter++
                payload.totalPrice = payload.counter * payload.currentPrice
                state.cartList.splice(indexItem, 1, payload)
                localStorage.setItem('cartProducts', JSON.stringify(state.cartList))
                return {...state, cartList: state.cartList}
            }

        case REMOVE_CART:
            const indexRemove = state.cartList.findIndex(item => item.itemNo === payload.itemNo)
            state.cartList.splice(indexRemove, 1)
            localStorage.setItem('cartProducts', JSON.stringify(state.cartList))
            return {...state, cartList: state.cartList}

        case CLEAR_CART:
            localStorage.removeItem('cartProducts')
            return {...state, cartList: []}

        case MINUS_ITEM:
            const indexMinus = state.cartList.findIndex(item => item.itemNo === payload.itemNo)
            if (state.cartList[indexMinus].counter === 1) {
                state.cartList.splice(indexMinus, 1)
                localStorage.setItem('cartProducts', JSON.stringify(state.cartList))
                return {...state, cartList: state.cartList}
            } else {
                payload.counter--
                payload.totalPrice = payload.counter * payload.currentPrice
                state.cartList.splice(indexMinus, 1, payload)
                localStorage.setItem('cartProducts', JSON.stringify(state.cartList))
                return {...state, cartList: state.cartList}
            }
        case PLUS_ITEM:
            const indexPlus = state.cartList.findIndex(item => item.itemNo === payload.itemNo)
            if (indexPlus === -1) {
                payload.counter = 1
                payload.totalPrice = payload.counter * payload.currentPrice
                localStorage.setItem('cartProducts', JSON.stringify(state.cartList))
                return {...state, cartList: [...state.cartList, payload]}
            } else {
                payload.counter++
                payload.totalPrice = payload.counter * payload.currentPrice
                state.cartList.splice(indexPlus, 1, payload)
                localStorage.setItem('cartProducts', JSON.stringify(state.cartList))
                return {...state, cartList: state.cartList}
            }
        case ADD_FROM_LOCALSTORAGE:
            return {...state, cartList: payload}

        default:
            return state
    }
}
