import {MINUS_ITEM} from '../constants';

export const minusItem = (payload) =>{
    return {
        type: MINUS_ITEM,
        payload
    }
}
