import {ADD_CART} from '../constants';

export const addCart = (payload) =>{
    return {
        type: ADD_CART,
        payload
    }
}
