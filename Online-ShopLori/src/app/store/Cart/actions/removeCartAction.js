import {REMOVE_CART} from '../constants';

export const removeCart = (payload) =>{
    return {
        type: REMOVE_CART,
        payload
    }
}
