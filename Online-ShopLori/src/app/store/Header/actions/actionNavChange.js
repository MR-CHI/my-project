import {CHANGE_NAV} from "../constants";

export const actionNavChange = (payload) =>{
    return {
        type: CHANGE_NAV,
    }
}
