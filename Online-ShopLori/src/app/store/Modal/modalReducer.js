import {initialState} from "./initialState";
import {CLOSE_MODAL, OPEN_MODAL} from "./constants";

export const modalReducer = (state= initialState, {type, payload})=> {
    switch(type) {
        case OPEN_MODAL:
            return {...state, modalIsOpen: true, product: payload};
        case CLOSE_MODAL:
            return {...state, modalIsOpen: false, product: []};
        default:
            return state;
    }
};
