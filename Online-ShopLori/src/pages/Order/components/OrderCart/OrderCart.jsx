import React from 'react';
import './OrderCart.scss'

const OrderCart = ({cart}) => {
    let allTotalPrice = 0;
    cart.forEach(({totalPrice}) => {
        allTotalPrice += totalPrice
    })
    const cartItems = cart.map(({imageUrls, name, itemNo, color, totalPrice}) => {
        return (
            <div className={'order-cart-item'}>
                <div className={'order-cart-item-view'}>
                    <img src={imageUrls[0]} alt={name}/>
                </div>
                <div className={'order-cart-item-description'}>
                    <span className={'order-cart-item-description-name'}>{name}</span>
                    <span className={'order-cart-item-description-itemNo'}>{itemNo}</span>
                    <span className={'order-cart-item-description-color'}>Цвет: {color}</span>
                </div>
                <div className={'order-cart-item-price'}>
                        <span className={'order-cart-item-price-total'}>{totalPrice} грн</span>
                </div>
            </div>
        )
    })
    return (
        <div className={'order-cart'}>
            <div className={'order-cart-header'}><h2>Товары в корзине</h2>
            </div>
            {cartItems}
            <div className={'order-cart-sum'}>
                    <span className={'order-cart-sum-label'}>Сумма заказа </span>
                <span className={'order-cart-sum-price'}>{allTotalPrice}</span>
            </div>
        </div>
    );

};

export default OrderCart;
