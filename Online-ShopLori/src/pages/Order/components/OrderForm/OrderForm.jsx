import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {useDispatch, useSelector} from "react-redux";
import {openModal} from "../../../../app/store/Modal/actions/openModal";
import Thank from "../../../../components/Thank";

const OrderForm = () => {
    const cart= useSelector(state=>state.cart.cartList)
    const dispatch = useDispatch()
    return(
    <div>
        <Formik
            initialValues={{ name: '', number: '', email:'', }}
            validate={values => {
                const errors = {};
                if (!values.name ) {
                    errors.name = 'Поле, обязательное к заполнению';
                } else if(!values.number){
                    errors.number = 'Поле, обязательное к заполнению';
                }else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Неправильниий e-mail';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                    console.log(cart, JSON.stringify(values, null, 2));
                    dispatch(openModal(<Thank/>))
            }}>
            {({ isSubmitting , values}) => (
                <>
                <Form className={'form-fields'}>
                    <label className={'form-label'} htmlFor="name">Імя получателя*</label>
                    <Field id={'name'} type="name" name="name" placeholder={'Імя получателя'} />
                    <ErrorMessage name="name" component="div" />

                    <label className={'form-label'} htmlFor="number">Телефон*</label>
                    <Field id={'number'} type="number" name="number" placeholder={'Телефон'} />
                    <ErrorMessage name="number" component="div" />

                    <label className={'form-label'} htmlFor="email">E-mail</label>
                    <Field type="email" name="email" placeholder={'e-mail'} />
                    <ErrorMessage name="email" component="div" />
                </Form>
                    <Form className={'form-radio'}>
                        <h3 className={'form-radio-heading'} id="my-radio-group">Доставка та оплата</h3>
                        <div className={'form-radio-group'} role="group" aria-labelledby="my-radio-group">
                        <p>Способ доставки</p>
                            <label>
                                <Field type="radio" name="delivery" value="courier" checked={true}/>
                                Курьером додому
                            </label>
                            <label>
                                <Field type="radio" name="delivery" value="pickup" />
                                Самовывоз
                            </label>
                        </div>
                        <div className={'form-radio-group'} role="group" aria-labelledby="my-radio">
                            <p>Способ оплаты</p>
                            <label>
                                <Field type="radio" name="payment" value="online" checked={true} />
                                Банковской картой онлайн
                            </label>
                            <label>
                                <Field type="radio" name="payment" value="in-place" />
                                Наличными или картой при получении
                            </label>
                        </div>

                        <button className={'form-btn'} type="submit">Подтвердить заказ</button>
                    </Form>
                </>
            )}
        </Formik>
    </div>
    )
};

export default OrderForm;
