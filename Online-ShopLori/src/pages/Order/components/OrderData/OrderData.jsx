import React from 'react';
import './OrderData.scss'
import OrderForm from "../OrderForm";

const OrderData = () => {
    return (
        <div className={'order-register'}>
            <div className={'order-register-header'}>
                <h2>Оформление заказа</h2>
                <span>Пожалуйста, заполните форму.
Выберите способ оплаты и способ доставки</span>
            </div>
            <div className={'order-register-form'}>
                <h3 className={'order-register-form-header'}>Контактные данные</h3>
                <OrderForm/>
            </div>
        </div>
    );
};

export default OrderData;
