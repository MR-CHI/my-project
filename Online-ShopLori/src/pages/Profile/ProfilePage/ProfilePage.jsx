import React, {useReducer} from "react";
import './ProfilePage.scss'
import Sidebar from "../components/Sidebar";
import {tabProps} from './tabProps'
import {profileReducer} from "../../../app/store/Profile/profileReducer";
import ContentPage from "../components/ContentPage";
import {useSelector} from "react-redux";

const ProfilePage = () => {
    const [state, dispatch] = useReducer(profileReducer, tabProps);
    const personalDetails = useSelector(state => state.addNewCustomer.user);
    const tabItems = state.map(item => {
        return (
            <span
                onClick={() => dispatch({ type: "ACTIVATE_TAB", payload: item.name })}
                className={item.active ? "user-setting-navbar active" : "user-setting-navbar"}>
                {item.label}

            </span>
        );
    });
        const activeTab = state.find(item => item.active === true)
    return (
        <section className='containerUserProfile'>
            <h2 className='title'>{personalDetails.firstName}, Ваш Личный кабинет</h2>
            <div className='user'>
                <Sidebar tabItems={tabItems}/>
                <ContentPage activeTab={activeTab} />
            </div>
        </section>
    )
}

export default ProfilePage
