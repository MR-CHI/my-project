import React, {useState} from "react";
import './UserProfile.scss'
import {userProfileFields} from './userProfileFields'
import Input from "../../../../components/Input/Input";
import axios from "axios";
import {useDispatch} from "react-redux";
import {addNewUser} from "../../../../app/store/Auth/actions/addNewuser";
import {useHistory} from "react-router-dom";

const UserProfile = () => {
    const token = localStorage.getItem('token');
    const history = useHistory()
    const dispatch = useDispatch();
    const [inputValue, setInputValue] = useState({});
    const handleChange = event =>{
        const {name, value} = event.target;
        inputValue[name] = value;
        setInputValue(inputValue);
    };

    const handleSubmit = (event)=>{
        event.preventDefault();
        const sendRequest = async () =>{
            try {
                const resp = await axios.put('https://glacial-river-19645.herokuapp.com/api/customers/', inputValue,
                    {headers: {'Authorization': JSON.parse(token) }})
                setInputValue({});
                alert('Изменения прошли успешно')
                dispatch(addNewUser(resp.data));
                localStorage.setItem('customer', JSON.stringify(resp.data))
                history.push('/HomePage')

            } catch (error){
                console.dir(error)
                alert(error.response.data.message && error.response)
            }
        }
        return sendRequest();
    };

    return (
        <div className='user-profile-section'>
            <h2 className='user-profile-label'>Заполните форму для изменения данных</h2>
            <form className='user-profile' onSubmit={handleSubmit} >
                <Input  {...userProfileFields.firstName} onChange = {handleChange}/>
                <Input  {...userProfileFields.lastName} onChange = {handleChange} />
                <Input  {...userProfileFields.login} onChange = {handleChange} />
                <Input  {...userProfileFields.email} onChange = {handleChange} />
                <Input className='buttonAuth' {...userProfileFields.submit}/>
            </form>

        </div>
    )
}

export default UserProfile
