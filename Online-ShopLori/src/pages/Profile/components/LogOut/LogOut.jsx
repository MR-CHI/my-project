import React from "react";
import Button from "../../../../components/Button/Button";
import {useDispatch} from "react-redux";
import {removeDataUser} from "../../../../app/store/Auth/actions/removeDataUser";
import {useSelector} from "react-redux";

const LogOut = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.addNewCustomer.user);
    const dataUser = () =>{
        dispatch(removeDataUser({}));
        localStorage.removeItem('customer')
    }

    return(
        <Button onClick={dataUser} text='Выйти' />

    )

}

export default LogOut
