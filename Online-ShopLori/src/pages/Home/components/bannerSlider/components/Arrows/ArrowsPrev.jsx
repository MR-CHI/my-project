import React from "react";

const ArrowsPrev = ({className, style, onClick}) => {

    return (
        <div className={className} style={{...style,
            // position: 'absolute',
            zIndex: '1001',
            width: '60',
            height: '200px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            // display: "block",
            background: '#BEBABA',
            margin: '0 0 0  25px',
            fontSize: '30px',
            opacity: '0.4',
            borderRadius: '0px 7px 7px 0px',



        }} onClick={onClick}/>
    );
};

export default ArrowsPrev;