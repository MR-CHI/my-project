import React from "react";

const ArrowsNext = ({className, style, onClick}) => {

    return (
        <div className={className} style={{...style,
            position: 'absolute',
            width: '60',
            height: '200px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            // display: "block",
            background: '#BEBABA',
            margin: '0 24px 0  0',
            fontSize: '30px',
            opacity: '0.4',
            borderRadius: '7px 0px 0px 7px',



        }} onClick={onClick}/>
    );
};

export default ArrowsNext;