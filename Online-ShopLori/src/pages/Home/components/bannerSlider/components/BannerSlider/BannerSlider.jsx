import React from 'react';
import {imagesList} from "../../imagesList";
import Slider from "react-slick";
import '../Arrows/styleArrows.scss'
import ArrowsNext from "../Arrows/ArrowsNext";
import SliderContainer from "../SliderContainer/SliderContainer";
import './styleSliderBanner.scss'
import ArrowsPrev from "../Arrows/ArrowsPrev";
import {v4 as uuidv4} from "uuid";

const  BannerSlider = () => {
    const banners = imagesList.map(({src, title, text, descriptions}) => <SliderContainer
        key={uuidv4()}
        sliderImgStyle='sliderImgStyle'
        className='content-slider'
        classNameTitle='title-slider'
        classNameText='discription-slider'
        classNameDescriptions='discription-slider-p'
        src={src}
        title={title}
        text={text}
        descriptions={descriptions}/>)

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        // arrows: true,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: <ArrowsPrev />,
        nextArrow: <ArrowsNext />,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    dots: true,
                    autoplaySpeed: 2000,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    dots: false,
                    arrows: false,
                    autoplaySpeed: 2000,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    dots: false,
                    arrows: false,
                    autoplaySpeed: 3000,
                }

            },
            {
                breakpoint: 320,
                settings: {
                    dots: false,
                    arrows: false,
                    autoplaySpeed: 3000,

                }
            }

        ]
        // nextArrow: <ArrowsNext className='arrows-next' />
    };
    return (
        <div className='container-slider-banner'>
            <Slider {...settings}>
                {banners}
            </Slider>
        </div>

    );
}
export default BannerSlider
