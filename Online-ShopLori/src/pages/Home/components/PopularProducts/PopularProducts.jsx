import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {productsRequestCreator} from "../../../../app/store/Products/actions/actionCreators";
import CardProduct from "../../../Products/components/CardProduct/CardProduct";
import './popularProductStyle.scss'
import Slider from "react-slick";
import {openModal} from "../../../../app/store/Modal/actions/openModal";
import ProductPage from "../../../Products/ProductPage";
import {v4 as uuidv4} from "uuid";

const PopularProducts = ({titleText}) =>{
    const dispatch = useDispatch();
    useEffect(() =>{
        const action = productsRequestCreator();
        dispatch(action)
    }, [])
    const loading = useSelector(state => state.allProduct.loading);
    const show = loading ? {display: 'none'} : {display: 'block'};
    const products = useSelector(state => state.allProduct.product);
    const cardsPopular = products.filter(({popular}) => popular === true);
    const result = cardsPopular.map((item) => {
        const {name, imageUrls, currentPrice,categories,itemNo} = item
        return(
        <CardProduct key={uuidv4()} name={name} itemNo={itemNo} categories={categories} src={imageUrls[0]} price={currentPrice} flag={false}/>
        )
    }
    )

    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 1500,
        pauseOnHover: true,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,

                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplaySpeed: 2000,
                }

            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplaySpeed: 2000,
                }
            }

        ]
    };

    return(
        <div className='popularProducts-container'>
            <div className='popularProducts-container-title'>
                <h2 className='title-popular-product' style={show} >{titleText}</h2>
            </div>

                <Slider  {...settings}>
                    {result}
                </Slider>



            </div>

                )
                }

export default PopularProducts
