import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {productsRequestCreator} from "../../../../app/store/Products/actions/actionCreators";
import CardCategory from "../../../Products/components/CardCategory/CardCategory";
import './popularCategory.scss'
import {NavLink} from "react-router-dom";
import {v4 as uuidv4} from "uuid";

const PopularCategory = () => {

    const dispatch = useDispatch();
    useEffect(() => {
        const action = productsRequestCreator();
        dispatch(action)
    }, [])
    const products = useSelector(state => state.allProduct.product);
    const cardsPopular = products.filter(({popular}) => popular === true);
    const loading = useSelector(state => state.allProduct.loading);
    const show = loading ? {display: 'none'} : {display: 'block'};
    const result = cardsPopular.map(({
                                         categories,
                                         description,
                                         imageUrls,
                                         productUrl
                                     }) => {
        return (
            <NavLink to={`/Products${productUrl}`} key={uuidv4()} >
                <CardCategory  description={description} src={imageUrls[1]}
                              categories={categories}/></NavLink>
        )
    })

    return (
            <div className='popular-category'>
                <div className='popular-category-title'>
                    <h2 style={show}>Популярные категории</h2>
                </div>
                <div className='popular-category-main'>
                    {result}
                </div>
            </div>
    )

}
export default PopularCategory
