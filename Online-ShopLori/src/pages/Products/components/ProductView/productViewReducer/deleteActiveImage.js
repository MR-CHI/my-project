import {DELETE_ACTIVE_IMAGE} from "./constants";

export const deleteActiveImage = (payload) => {
    return{
        type: DELETE_ACTIVE_IMAGE,
    }
}
