import React from "react";
import './ProductCharacteristics.scss'

const ProductCharacteristics = () => {
    return(
        <div className='product-characteristics'>
            <h3 className='product-characteristics-heading'>Характеристики</h3>
            <span
                className='product-characteristics-type'>Покриття</span><span
            className='product-characteristics-description'>Більше 20 варіантів фарбування (лляна олія, олія-віск, безбарвний лак, тонований лак, RAL)</span>
            <span
                className='product-characteristics-type'>Сидіння</span><span
            className='product-characteristics-description'>Дерев'яне або м'яке</span>
            <span
                className='product-characteristics-type'>Оббивка</span><span
            className='product-characteristics-description'>Тканина або шкірзам на вибір наступних торгових марок: «ЕксімТекстіль», «Аппарель»</span>
            <span
                className='product-characteristics-type'>Упаковка</span><span
            className='product-characteristics-description'>Картонна коробка: 2шт. в 1 коробці;<br/>Вага брутто коробки – 16 кг;<br/>Об'єм коробки – 0,33м3;<br/>Розмір коробки – 62х68х120см</span>
        </div>
    )
}

export default ProductCharacteristics
