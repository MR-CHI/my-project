import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import CardProduct from "../CardProduct/CardProduct";
import {addCart} from "../../../../app/store/Cart/actions/addCartAction";

const AllProducts = ({category}) => {
    const state = useSelector(state => state.allProduct);
    const products = state.product;
    const categoryProducts = products.filter(item => item.categories===category);
    const categoryFilterProducts = state.filterProduct.filter(item => item.categories===category);
    const dispatch= useDispatch();
    const result = state.filterProduct.length === 0 ? categoryProducts : categoryFilterProducts;

    const allProducts = result.map((item) => {
        const {currentPrice, name, imageUrls,categories,itemNo}= item;
        return (
            <>
                <CardProduct
                        flag={true}
                        categories={categories}
                        onclickToCart={()=>dispatch(addCart(item))}
                        src={imageUrls[0]}
                        name={name}
                        price={currentPrice}
                        itemNo={itemNo}
                        text={'Добавить'}/>
                </>

        )
    })

    return allProducts
};

export default AllProducts;
