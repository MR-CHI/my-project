import React from "react";
import Button from "../../../../components/Button/Button";
import './cardCategoryStyle.scss'

const CardCategory = ({src, description, className = '', alt='photo', categories}) =>{

    return(
        <div className='popularCategory-container' >
            <img className='categoryCardImage'  src={src} alt={alt}/>
            <div className='popularCategory-container_block'>
                <p className='popularCategory-container_item'>{categories}</p>
                <p className='popularCategory-container_description'>{description}</p>
                <Button className='popularCategory-container_button' text='Подробнее' />
            </div>
        </div>
    )
}

export default CardCategory
