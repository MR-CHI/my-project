import React from 'react';
import './CartPage.scss'
import {useSelector} from "react-redux";
import CartList from "../components/CartList";
import Button from "../../../components/Button";
import {NavLink} from "react-router-dom";

const CartPage = () => {
    const state = useSelector(state => state.cart)
    const cart = state.cartList
    let allTotalPrice = 0;
    cart.forEach(({totalPrice}) => {
        allTotalPrice += totalPrice
    })

    return (
        <div className={'cart'}>
            <div className={'cart-header'}>
                <span className={'cart-header-label'}>Название товара</span>
                <span className={'cart-header-label'}>Количество</span>
                <span className={'cart-header-label'}>Цена</span>
            </div>
            <CartList cart={cart}/>
            <div className={'cart-footer'}>
                <span
                    className={'cart-footer-counter'}>Всего в корзине {cart.length} товаров на сумму <strong
                    className={'cart-footer-total'}>{allTotalPrice} грн</strong></span>
                <NavLink to={'/orderingPage'}>
                    <Button className={'cart-footer-btn'}
                            text={'Оформить заказ'}/>
                </NavLink>
            </div>
        </div>
    );
};

export default CartPage;
