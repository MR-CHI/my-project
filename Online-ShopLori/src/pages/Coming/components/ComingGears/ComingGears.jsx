import React from 'react';
import './ComingGears.scss';

const ComingGears = () => {
    return(
        <section className='containerNoPage'>
            <div className={'gears'}>
                <div className="loader">
                    <div className="cogs">
                        <div className="cog cog0">
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                        </div>
                        <div className="cog cog1">
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                        </div>
                        <div className="cog cog2">
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                            <div className="bar"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default ComingGears;
