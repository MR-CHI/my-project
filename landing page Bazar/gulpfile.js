const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    minifyjs = require('gulp-js-minify'),
    uglify = require('gulp-uglify'),
    cleancss = require('gulp-clean-css'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    autoprefixer = require('gulp-autoprefixer');
const cleanDist = () => {
    return gulp.src('dist', {read: false})
        .pipe(clean());
};
const scssBuild = () => {
    return gulp.src('src/scss/main.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleancss({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css/'));
};
const jsBuild = () => {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'))
};
const imagesBuild = () => {
    return gulp.src('src/img/**')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img/'))
};
const watch = () => {
    gulp.watch('src/scss/**', scssBuild).on('change', browserSync.reload);
    gulp.watch('src/img/**/*.*').on('change', browserSync.reload);
    gulp.watch('src/js/*.js', jsBuild).on('change', browserSync.reload);
    gulp.watch('**.html').on('change', browserSync.reload);
    browserSync.init({
        server: {
            baseDir: "./"
        }
    })
};
gulp.task('dev', watch);
gulp.task('build', gulp.series(cleanDist, scssBuild, jsBuild, imagesBuild));


