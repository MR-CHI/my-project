import {Component} from "../component.js";

export class Textarea extends Component{
    render(){
        const {errorText, ...attr} = this.props;
        const textarea = this.createElement('textarea', attr);
        return textarea;
    }

}