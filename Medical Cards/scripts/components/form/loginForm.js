import {Form} from "./form.js";
import {Input} from "../input/input.js";
import {request} from "../axios/axios.js";
import {CreateVisitBtn} from "../buttons/createVisit.js";
import {ModalAddVisit} from "../modal/modalAddVisit.js";
import {VisitDentist} from "../card/visitDentist.js";
import {VisitCardiologist} from "../card/visitCardiologist.js";
import {VisitTherapist} from "../card/visitTherapist.js";
import {SelectDocFilt} from "../select/selectDocFilt.js";
import {SelectPrioriryFilt} from "../select/selectPrioriryFilt.js";
import {FilterBtn} from "../buttons/find.js";
import {ResetBtn} from "../buttons/reset.js";

export class LoginForm extends Form {
    emailProps = {
        type: "email",
        name: "user-email",
        required: true,
        className: "form-control",
        placeholder: "Введите email",
        errorText: "Enter email!"
    }
    passwordProps = {
        type: "password",
        name: "user-password",
        required: true,
        className: "form-control",
        placeholder: "Введите password",
        errorText: "Enter password!"
    }
    submitProps = {
        type: "submit",
        className: 'form-submit'
    }
    createVisitProps = {
        className: 'create-visit-btn',
        content: 'Create visit'
    }

    render() {
        const {emailProps, passwordProps, submitProps, createVisitProps} = this
        const emailInput = new Input(emailProps)
        const passwordInput = new Input(passwordProps)
        const submitInput = new Input(submitProps)
        const form = super.render(this.props)
        form.append(emailInput.render(), passwordInput.render(), submitInput.render())
        form.addEventListener('submit', function (e) {
            e.preventDefault()
            const email = this.querySelector('[name="user-email"]').value
            const password = this.querySelector('[name="user-password"]').value
            const body = {email, password}
            if (email && password) {
                request.post('/login', body)
                    .then(({data}) => {
                        if (data.status === 'Success') {
                            localStorage.setItem('token', data.token)
                            this.parentElement.remove()
                            const btn = document.querySelector('.header-buttons-element')
                            btn.remove()
                            const createBtn = new CreateVisitBtn(createVisitProps)
                            const btnCreate = createBtn.render()
                            const container = document.querySelector('.headers-button')
                            const selectDoc = new SelectDocFilt().render()
                            const selectPr = new SelectPrioriryFilt().render()
                            const filterBtn = new FilterBtn().render()
                            const resetBtn = new ResetBtn().render()
                            const header = document.querySelector('.headers')
                            header.append(selectDoc,selectPr, filterBtn, resetBtn)
                            container.append(btnCreate)
                            btnCreate.addEventListener('click', function () {
                                const visitModal = new ModalAddVisit()
                                const container = document.querySelector('.cards')
                                container.append(visitModal.render())
                            })
                            request.get('/cards').then(({data}) => {
                                const container = document.querySelector('.cards')
                                if(data.length === 0){
                                    const text = document.createElement("p")
                                    text.className = 'empty-text'
                                    text.textContent = 'No items have been added'
                                    container.append(text)
                                }
                                data.forEach(el => {
                                    if (el.doctor === 'dentist') {
                                        const newVisit = new VisitDentist()
                                        newVisit.visitProps = el
                                        const card = newVisit.render()
                                        container.append(card)
                                        card.setAttribute('data-id', el.id)
                                    }
                                    if (el.doctor === 'cardiologist') {
                                        const newVisit = new VisitCardiologist()
                                        newVisit.visitProps = el
                                        const card = newVisit.render()
                                        container.append(card)
                                        card.setAttribute('data-id', el.id)
                                    }
                                    if(el.doctor === 'therapist'){
                                        const newVisit = new VisitTherapist()
                                        newVisit.visitProps = el
                                        const card = newVisit.render()
                                        container.append(card)
                                        card.setAttribute('data-id', el.id)
                                    }
                                })
                            })
                        } else {
                            const error = document.createElement('p')
                            error.className = 'error-msg'
                            error.textContent = data.message
                            const input = form.querySelector('[name="user-password"]')
                            input.after(error)
                        }
                        console.log(data)
                    })
                    .catch(error => console.log(error))

            } else {
                const error = document.createElement('p')
                error.textContent = 'Email and password should be filled in'
                const input = form.querySelector('[name="user-password"]')
                input.after(error)
            }
        })
        this.elem = form
        return form
    }
}
